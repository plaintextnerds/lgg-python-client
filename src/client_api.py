"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""
from typing import Dict, List
from urllib.parse import urljoin

import requests
from nacl.encoding import Base64Encoder  # type: ignore
from nacl.signing import VerifyKey  # type: ignore


def generate_ident(
    api: str,
    ip: str,
    token: str,
) -> str:
    response = requests.get(
        urljoin(api, f"ident/{ip}"),
        headers={"Authorization": f"Bearer {token}"},
    )
    response.raise_for_status()
    data = response.json()
    if len(data["ident"]) > 88:
        raw_signature = Base64Encoder.decode(data["ident"])[:64]
        key = VerifyKey(data["pub"], encoder=Base64Encoder)
        key.verify(ip.encode(), raw_signature)
        return Base64Encoder.encode(raw_signature)
    return data["ident"]


def get_providers(
    api: str,
    token: str,
) -> List[Dict]:
    response = requests.get(
        urljoin(api, "provider"),
        headers={"Authorization": f"Bearer {token}"},
    )
    response.raise_for_status()
    return response.json()


def get_provider(api: str, token: str, provider_name: str) -> Dict:
    response = requests.get(
        urljoin(api, f"provider/{provider_name}"),
        headers={"Authorization": f"Bearer {token}"},
    )
    response.raise_for_status()
    return response.json()


def get_metrics(
    api: str, token: str, provider_name: str, location_name: str, ip: str
) -> Dict:
    response = requests.get(
        urljoin(api, f"metrics/{provider_name}/{location_name}/{ip}"),
        headers={"Authorization": f"Bearer {token}"},
    )
    response.raise_for_status()
    return response.json()
